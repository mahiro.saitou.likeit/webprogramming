<%@ page language="java" contentType="text/html; charest=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>title</title>      
    <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
</head>
	<body>
        <header>
            <p align="right">ユーザー名さん
                <button type="button" class="btn btn-link">ログアウト</button>
            </p>
        </header>
        <h1 style="text-align: center;">ユーザー覧</h1>
        <br>
        <div align="right" ><button type="button" class="btn btn-link">新規登録</button>
        </div>
        <br>
        ログインID
        <input type="text" style="width:200px;">
        <br>
        <br>
        ユーザー名
        <input type="text" style="width:200px;">
        <br>
        <br>
        生年月日 
        <br>
        <div align="right">
        <button type="button" class="btn btn-secondary">検索</button>
        </div>
        <hr>
        <table class="table table-sm">
            <thead>
              <tr>
                <th scope="col">ログインID</th>
                <th scope="col">ユーザー名</th>
                <th scope="col">生年月日</th>
                <th scope="col"></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th scope="row">id0001</th>
                <td>田中太郎</td>
                <td>1989年04月26日</td>
                <td >
                    <button type="button" class="btn btn-primary">詳細</button>
                    <button type="button" class="btn btn-success">更新</button>
                    <button type="button" class="btn btn-danger">削除</button>
                </td>
              </tr>
              <tr>
                <th scope="row">id0002</th>
                <td>佐藤二郎</td>
                <td>2001年11月12日</td>
                <td>
                    <button type="button" class="btn btn-primary">詳細</button>
                    <button type="button" class="btn btn-success">更新</button>
                    <button type="button" class="btn btn-danger">削除</button>
                </td>
              </tr>
              <tr>
                <th scope="row">id0002</th>
                <td>佐川真司</td>
                <td>2000年01月01日</td>
                <td>
                    <button type="button" class="btn btn-primary">詳細</button>
                    <button type="button" class="btn btn-success">更新</button>
                    <button type="button" class="btn btn-danger">削除</button>
                </td>
              </tr>
            </tbody>
          </table>
    </body>
</html>